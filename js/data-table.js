var dataSet = [
  [
    "Tiger Nixon",
    "System Architect",
    "Edinburgh",
    "5421",
    "2011/04/25",
    "$320,800",
  ],
  ["Garrett Winters", "Accountant", "Tokyo", "8422", "2011/07/25", "$170,750"],
  [
    "Ashton Cox",
    "Junior Technical Author",
    "San Francisco",
    "1562",
    "2009/01/12",
    "$86,000",
  ],
  [
    "Cedric Kelly",
    "Senior Javascript Developer",
    "Edinburgh",
    "6224",
    "2012/03/29",
    "$433,060",
  ],
  ["Airi Satou", "Accountant", "Tokyo", "5407", "2008/11/28", "$162,700"],
  [
    "Brielle Williamson",
    "Integration Specialist",
    "New York",
    "4804",
    "2012/12/02",
    "$372,000",
  ],
  [
    "Herrod Chandler",
    "Sales Assistant",
    "San Francisco",
    "9608",
    "2012/08/06",
    "$137,500",
  ],
  [
    "Rhona Davidson",
    "Integration Specialist",
    "Tokyo",
    "6200",
    "2010/10/14",
    "$327,900",
  ],
  [
    "Colleen Hurst",
    "Javascript Developer",
    "San Francisco",
    "2360",
    "2009/09/15",
    "$205,500",
  ],
  [
    "Sonya Frost",
    "Software Engineer",
    "Edinburgh",
    "1667",
    "2008/12/13",
    "$103,600",
  ],
  ["Jena Gaines", "Office Manager", "London", "3814", "2008/12/19", "$90,560"],
  [
    "Quinn Flynn",
    "Support Lead",
    "Edinburgh",
    "9497",
    "2013/03/03",
    "$342,000",
  ],
  [
    "Charde Marshall",
    "Regional Director",
    "San Francisco",
    "6741",
    "2008/10/16",
    "$470,600",
  ],
  [
    "Haley Kennedy",
    "Senior Marketing Designer",
    "London",
    "3597",
    "2012/12/18",
    "$313,500",
  ],
  [
    "Tatyana Fitzpatrick",
    "Regional Director",
    "London",
    "1965",
    "2010/03/17",
    "$385,750",
  ],
  [
    "Michael Silva",
    "Marketing Designer",
    "London",
    "1581",
    "2012/11/27",
    "$198,500",
  ],
  [
    "Paul Byrd",
    "Chief Financial Officer (CFO)",
    "New York",
    "3059",
    "2010/06/09",
    "$725,000",
  ],
  [
    "Gloria Little",
    "Systems Administrator",
    "New York",
    "1721",
    "2009/04/10",
    "$237,500",
  ],
  [
    "Bradley Greer",
    "Software Engineer",
    "London",
    "2558",
    "2012/10/13",
    "$132,000",
  ],
  ["Dai Rios", "Personnel Lead", "Edinburgh", "2290", "2012/09/26", "$217,500"],
  [
    "Jenette Caldwell",
    "Development Lead",
    "New York",
    "1937",
    "2011/09/03",
    "$345,000",
  ],
  [
    "Yuri Berry",
    "Chief Marketing Officer (CMO)",
    "New York",
    "6154",
    "2009/06/25",
    "$675,000",
  ],
  [
    "Caesar Vance",
    "Pre-Sales Support",
    "New York",
    "8330",
    "2011/12/12",
    "$106,450",
  ],
  [
    "Doris Wilder",
    "Sales Assistant",
    "Sydney",
    "3023",
    "2010/09/20",
    "$85,600",
  ],
  [
    "Angelica Ramos",
    "Chief Executive Officer (CEO)",
    "London",
    "5797",
    "2009/10/09",
    "$1,200,000",
  ],
  ["Gavin Joyce", "Developer", "Edinburgh", "8822", "2010/12/22", "$92,575"],
  [
    "Jennifer Chang",
    "Regional Director",
    "Singapore",
    "9239",
    "2010/11/14",
    "$357,650",
  ],
  [
    "Brenden Wagner",
    "Software Engineer",
    "San Francisco",
    "1314",
    "2011/06/07",
    "$206,850",
  ],
  [
    "Fiona Green",
    "Chief Operating Officer (COO)",
    "San Francisco",
    "2947",
    "2010/03/11",
    "$850,000",
  ],
  [
    "Shou Itou",
    "Regional Marketing",
    "Tokyo",
    "8899",
    "2011/08/14",
    "$163,000",
  ],
  [
    "Michelle House",
    "Integration Specialist",
    "Sydney",
    "2769",
    "2011/06/02",
    "$95,400",
  ],
  ["Suki Burks", "Developer", "London", "6832", "2009/10/22", "$114,500"],
  [
    "Prescott Bartlett",
    "Technical Author",
    "London",
    "3606",
    "2011/05/07",
    "$145,000",
  ],
  [
    "Gavin Cortez",
    "Team Leader",
    "San Francisco",
    "2860",
    "2008/10/26",
    "$235,500",
  ],
  [
    "Martena Mccray",
    "Post-Sales support",
    "Edinburgh",
    "8240",
    "2011/03/09",
    "$324,050",
  ],
  [
    "Unity Butler",
    "Marketing Designer",
    "San Francisco",
    "5384",
    "2009/12/09",
    "$85,675",
  ],
];

let cloneObj = [...dataSet];

var dataHeader = [
  {
    key: 1,
    value: "Name",
    status: "sorting_asc",
  },
  {
    key: 2,
    value: "Position",
  },
  {
    key: 3,
    value: "Office",
  },
  {
    key: 4,
    value: "Extn.",
  },
  {
    key: 5,
    value: "Start date",
  },
  {
    key: 6,
    value: "Salary",
  },
];


const dataTable = document.getElementById("data-table");
const headerTable = document.getElementById("data-table__header");
const bodyTable = document.getElementById("data-table__body");
const selectValue = document.getElementById("select-value");
const inputValue = document.getElementById("input-text");
const paginationNumber = document.querySelector("#pagination-page-number");
let btns = paginationNumber.getElementsByClassName("btn");
const btnNext = document.querySelector(".btn-next");
const btnPrev = document.querySelector(".btn-prev");
const footerPagination = document.getElementById("pagination");

let headerData = "";
dataHeader.map((element) => {
  headerData += `<th class='header-table__sort ${
    element.status ? element.status : ""
  }'>${element.value}</th>`;
});
headerData = `<tr>${headerData}</tr>`;
headerTable.innerHTML = headerData;

selectValue.addEventListener("change", (e) => {
  limit = e.target.value;
  if (limit >= dataSet.length) {
    limit = dataSet.length;
  }
  loadData(limit, 1);
  setPagesPagination(limit);
});

function loadData(limit, page) {
  let start = (page - 1) * Number(limit);
  let end = ((start + Number(limit)) < dataSet.length) ? (start + Number(limit)) : dataSet.length ;

  let sliceArr = dataSet.slice(start, end);

  let bodyData = "";
  sliceArr.map((dataRow) => {
    let row = "";
    dataRow.map((dataCell) => {
      row += `<td>${dataCell}</td>`;
    });
    bodyData += `<tr>${row}</tr>`;
  });

  bodyTable.innerHTML = bodyData;
  footerPagination.innerHTML = `Showing ${start+1} to ${end} of ${dataSet.length} entries`

  if (page == 1) {
    btnPrev.classList.add("btn-disable");
  } else btnPrev.classList.remove("btn-disable");

  if (selectValue.value >= dataSet.length) {
    btnNext.classList.add("btn-disable");
  } else btnNext.classList.remove("btn-disable");
}

//search
inputValue.addEventListener("input", (e) => {
  e.preventDefault();
  let text = e.target.value.toUpperCase();
  let newArr = cloneObj.filter((item) => {
    let tpm = false;
    for (let i = 0; i < item.length; i++) {
      if (item[i].toUpperCase().includes(text)) tpm = true;
    }
    return tpm;
  });

  dataSet = [...newArr];

  console.log(dataSet);

  setPagesPagination(selectValue.value);
  loadData(selectValue.value, 1);

  if (dataSet.length < selectValue.value) {
    btnNext.classList.add("btn-disable");
  } else btnNext.classList.remove("btn-disable");

});


//set pagination
function setPagesPagination(limit) {
  var pages = Math.ceil(dataSet.length / limit);
  let pagesNumber = "";
  for (let number = 0; number < pages; number++) {
    pagesNumber += `<button  class="pagination-number btn pagination-btn"  >${
      number + 1
    }</button>`;
  }
  paginationNumber.innerHTML = pagesNumber;

  const allItems = document.querySelectorAll(".pagination-number");
  allItems[0] && allItems[0].classList.add("selected");
  var current = document.getElementsByClassName("selected");

  // add event click
  allItems.forEach((item, index) => {
    item.addEventListener("click", () => {
      loadData(limit, index + 1);

      // add style click button
      current[0].className = current[0].className.replace(" selected", "");
      item.classList.add("selected");

      if (current[0].textContent == pages) {
        btnNext.classList.add("btn-disable");
      } else btnNext.classList.remove("btn-disable");

      //end add style click button
    });
  });
  // end add event click
}

//handle Next Button
btnNext.addEventListener("click", () => {
  let btnList = [...paginationNumber.children];
  let index = 0;
  btnList.map((item, i) => {
    let itemList = [...item.classList];
    if (itemList.includes("selected")) index = i;
  });
  btnList[index].classList.remove("selected");
  btnList[index + 1].classList.add("selected");
  loadData(selectValue.value, index + 2);

  if (index + 2 == btnList.length) {
    btnNext.classList.add("btn-disable");
  } else btnNext.classList.remove("btn-disable");
});

//handle Prev Button
btnPrev.addEventListener("click", () => {
  let btnList = [...paginationNumber.children];
  let index = 0;
  btnList.map((item, i) => {
    let itemList = [...item.classList];
    if (itemList.includes("selected")) index = i;
  });
  btnList[index].classList.remove("selected");
  btnList[index - 1].classList.add("selected");
  loadData(selectValue.value, index);

  if (index == btnList.length) {
    btnNext.classList.add("btn-disable");
  } else btnNext.classList.remove("btn-disable");
});

window.onload = function () {
  setPagesPagination(selectValue.value);
  loadData(selectValue.value, 1);


};

//Sort
const itemHeader = document.getElementsByClassName("header-table__sort");
let itemSort = [...itemHeader];

const sortAtoZ = function (index) {
  dataSet.sort((a, b) => {
    if (a[index] < b[index]) {
      return -1;
    }
    if (a[index] > b[index]) {
      return 1;
    }
    return 0;
  });
};

const sortZtoA = function (index) {
  dataSet.sort((a, b) => {
    if (a[index] > b[index]) {
      return -1;
    }
    if (a[index] < b[index]) {
      return 1;
    }
    return 0;
  });
};

itemSort.forEach((item, index) => {
  item.addEventListener("click", () => {
    if (item.classList.contains("sorting_asc")) {
      item.classList.remove("sorting_asc");
      item.classList.add("sorting_dsc");
      console.log(index)
      sortAtoZ(index);
      loadData(selectValue.value, 1);
      setPagesPagination(selectValue.value);
    } else if (item.classList.contains("sorting_dsc")) {
      item.classList.remove("sorting_dsc");
      item.classList.add("sorting_asc");
      console.log(index)
      sortZtoA(index);
      loadData(selectValue.value, 1);
      setPagesPagination(selectValue.value);
    } else if (!item.classList.contains("sorting_asc")) {
      itemSort.forEach((sort) => {
        if (sort.classList.contains("sorting_asc")) {
          sort.classList.remove("sorting_asc");
          sortAtoZ(index);
          loadData(selectValue.value, 1);
          setPagesPagination(selectValue.value);
        } else
        sort.classList.remove('sorting_dsc')
      });
      item.classList.add("sorting_asc");
      sortZtoA(index);
      loadData(selectValue.value, 1);
      setPagesPagination(selectValue.value);
    }
  });
});

// current[0].className = current[0].className.replace(" selected", "");
//       item.classList.add("selected");

//       if (current[0].textContent == pages) {
//         btnNext.classList.add("btn-disable");
//       } else btnNext.classList.remove("btn-disable");
