const navbarShowBtn = document.getElementById("navbarToggleShow");
const menuSmallScreen = document.getElementById("menuSmallScreen");
const dropdownRequest = document.getElementById("dropdown-request");

for(let i = 0 ; i < dropdownRequest.children.length; i++ ){
  dropdownRequest.children[i].addEventListener('click', () => {
    let listActive = [...dropdownRequest.children[i].classList];
    if(listActive.includes('dropdown-click')) {
      dropdownRequest.children[i].classList.remove('dropdown-click');
    } else if (!listActive.includes('dropdown-click')){
      for( let j = 0 ; j < dropdownRequest.children.length ; j++){
        let list = [...dropdownRequest.children[j].classList];
        if(list.includes('dropdown-click')) {
          dropdownRequest.children[j].classList.remove('dropdown-click');
        }
      }
      dropdownRequest.children[i].classList.add('dropdown-click');
    }
  })
}



navbarShowBtn.addEventListener("click", () => {
  if (menuSmallScreen.classList.contains("menu-hidden")) {
    menuSmallScreen.classList.remove("menu-hidden");
    menuSmallScreen.classList.add("menu-active");
  }
});

const menuHidden = document.getElementById("menu-hidden");
const navbarToggleClose = document.getElementById("navbarToggleClose");
navbarToggleClose.addEventListener("click", () => {
  if (menuSmallScreen.classList.contains("menu-active")) {
    menuSmallScreen.classList.remove("menu-active");
    menuSmallScreen.classList.add("menu-hidden");
    menuHidden.style.animation = "animation: fadeOut ease-out 0.3s;";
  }
});

function reveal() {
  var reveals = document.querySelectorAll(".reveal");
  for (let i = 0; i < reveals.length; i++) {
    let windowHeight = window.innerHeight;
    let elementTop = reveals[i].getBoundingClientRect().top;
    let elementVisible = 150;
    if (elementTop < windowHeight - elementVisible) {
      reveals[i].classList.add("active");
    } else {
      reveals[i].classList.remove("active");
    }
  }
}

window.addEventListener("scroll", reveal);

// Increasing and decreasing number
const toggleBilling = document.getElementById("billing");
const numberBilling = document.getElementById("numberBilling");

toggleBilling.addEventListener("change", (e) => {
  let increasingNumber = 29;
  let decreasingNumber = 49;
  if (e.currentTarget.checked == true) {
    const myInterval = setInterval(() => {
      increasingNumber += 1;
      numberBilling.innerHTML = increasingNumber;
      if (increasingNumber == 49) clearInterval(myInterval);
    }, 20);
  } else {
    const myInterval = setInterval(() => {
      decreasingNumber -= 1;
      numberBilling.innerHTML = decreasingNumber;
      if (decreasingNumber == 29) clearInterval(myInterval);
    }, 20);
  }
});

// JS typewriter
var TxtType = function (el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10) || 2000;
  this.txt = "";
  this.tick();
  this.isDeleting = false;
};

TxtType.prototype.tick = function () {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">' + this.txt + "</span>";

  var objectType = this;
  var delta = 200 - Math.random() * 100;

  if (this.isDeleting) {
    delta /= 2;
  }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === "") {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function () {
    objectType.tick();
  }, delta);
};

window.onload = function () {
  var elements = document.getElementsByClassName("typewrite");
  for (var i = 0; i < elements.length; i++) {
    var toRotate = elements[i].getAttribute("data-type");
    var period = elements[i].getAttribute("data-period");
    if (toRotate) {
      new TxtType(elements[i], JSON.parse(toRotate), period);
    }
  }
  // INJECT CSS
  var css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = ".typewrite > .wrap { border-right: 0.15rem solid #42ba96}";
  document.body.appendChild(css);
};

// SLIDER
let slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides((slideIndex += n));
}

// Show Slide
function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("testimonials-image");
  let contents = document.getElementsByClassName("carousel-item");
  if (n > slides.length && n > contents.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length = contents.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
    contents[i].style.display = "none";
  }

  slides[slideIndex - 1].style.display = "block";
  contents[slideIndex - 1].style.display = "block";
}

// validation
const inputsForm = document.querySelectorAll(".form-scope input ");
const formButton = document.querySelector(".form-inner > button");

inputsForm.forEach((element) => {
  element.addEventListener("keyup", (e) => {
    element.classList.remove("value-invalid");
    var keycode = e.keyCode ? e.keyCode : e.which;
    if (keycode === 13) {
      e.preventDefault();
      formButton.click();
    }
  });
});

formButton.addEventListener("click", (e) => {
  e.preventDefault();
  const [name, email, password] = inputsForm;
  const errors = [];

  if (name.value.length === 0) {
    errors.push("Name is required !!! Not valid");
    name.classList.add("value-invalid");
  } else name.classList.remove("value-invalid");

  const re =
    /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  if (!re.test(email.value)) {
    errors.push("Email is not valid ");
    email.classList.add("value-invalid");
  } else email.classList.remove("value-invalid");

  if (password.value.length <= 7) {
    errors.push("Password is not valid");
    password.classList.add("value-invalid");
  } else password.classList.remove("value-invalid");

  if (errors.length === 0) console.log(name.value, email.value, password.value);
});



